import { Routes, RouterModule, Router } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { RegistrarRSMComponent } from './components/registrar-rsm/registrar-rsm.component';
import { ResolverSolicitudesService } from './services/resolver-solicitudes.service';
import { ListadoComponent } from './components/listado/listado.component';
import { RegistrarSMComponent } from './components/registrar-sm/registrar-sm.component';
import { ReporteSMComponent } from './components/reporte-sm/reporte-sm.component';


const APP_ROUTES: Routes = [
    {path:'login', component:LoginComponent},
    {path:'home/:usr', component:HomeComponent},
    {path:'regSM/:usr', component:RegistrarSMComponent},
    {path:'regRSM/:usr',component:RegistrarRSMComponent, resolve:{jefes:ResolverSolicitudesService}},
    {path:'reporteSM/:usr',component:ReporteSMComponent},
    {path:'listado/:usr',component:ListadoComponent, resolve:{jefes:ResolverSolicitudesService}},
    {path:'',pathMatch:'full',redirectTo:'login'},
    {path:'*',pathMatch:'full',redirectTo:'login'}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
