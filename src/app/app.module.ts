import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import{ APP_ROUTING } from '././app.routes'

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { RegistrarRSMComponent } from './components/registrar-rsm/registrar-rsm.component';
import { SolicitudesComponent } from './components/solicitudes/solicitudes.component';
import { TicketComponent } from './components/ticket/ticket.component';
import { ListadoComponent } from './components/listado/listado.component';
import { EstadosComponent } from './components/estados/estados.component';
import { RegistrarSMComponent } from './components/registrar-sm/registrar-sm.component';
import { ReporteSMComponent } from './components/reporte-sm/reporte-sm.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    NavbarComponent,
    RegistrarRSMComponent,
    SolicitudesComponent,
    TicketComponent,
    ListadoComponent,
    EstadosComponent,
    RegistrarSMComponent,
    ReporteSMComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
