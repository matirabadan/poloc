import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JefeProyecto } from 'src/app/clases/JefeProyecto';
import { Plan } from 'src/app/clases/Plan';
import { Ticket } from 'src/app/clases/Ticket';
import { GestorRegistrarResolucionDeSM } from 'src/app/services/GestorRegistrarResolucionDeSM';

@Component({
  selector: 'app-solicitudes',
  templateUrl: './solicitudes.component.html',
  styleUrls: ['./solicitudes.component.css']
})
export class SolicitudesComponent implements OnInit {

  @Input() jefe: JefeProyecto;
  @Input() idSolicitud: string;

  usuario: string;

  mostrandoTicket = false;

  tickets: Ticket[] = [];

  confirmando = false;

  notificando = false;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private solicitudesService: GestorRegistrarResolucionDeSM) {
    this.confirmando = false;
    this.mostrandoTicket = false;
    this.notificando = false;
    this.activatedRoute.params.subscribe(data => {
      this.usuario = data['usr'];
    })
  }

  ngOnInit() {
  }

  getUltimoPlan(): Plan {
    let proyectos = this.jefe.getProyectosQueDirige();
    for (let i = 0; i < proyectos.length; i++) {
      let solicitudes = proyectos[i].getSolicitudes();
      for (let j = 0; j < solicitudes.length; j++) {
        return solicitudes[j].getUltimoPlan();
      }
    }
  }

  mostrarTickets() {
    this.mostrandoTicket = true;
    console.log(this.tickets);

  }

  agregarTicket(ticket, seleccionado) {
    if (seleccionado) {
      this.tickets.push(ticket);
    } else {
      for (let i = 0; i < this.tickets.length; i++) {
        if (this.tickets[i] === ticket) {
          this.tickets.splice(i, 1);
        }
      }
    }
  }


  resolverSM() {
    this.confirmando = true;
  }

  finalizarSM() {
    this.solicitudesService.finalizarSM();
    this.notificando = true;
  }

  navegarAtras() {
    if (this.mostrandoTicket) {
      this.tickets = [];
      this.mostrandoTicket = false;
    } else {
      if (this.confirmando) {
        this.tickets = [];
        this.confirmando = false
      } else {
        this.router.navigateByUrl(`/home/${this.usuario}`);
      }
    }
  }

}
