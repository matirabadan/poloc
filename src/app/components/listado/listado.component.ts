import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JefeProyecto } from 'src/app/clases/JefeProyecto';
import { SolicitudMantenimiento } from 'src/app/clases/SolicitudMantenimiento';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {

  usuario: string;
  visualizandoHistorial = false;

  jefes: JefeProyecto[] = [];

  solicitud:SolicitudMantenimiento;

  constructor(private activatedRoute: ActivatedRoute, private router: Router) {
    this.visualizandoHistorial = false;
    this.activatedRoute.params.subscribe(data => { this.usuario = data['usr'] });

    this.activatedRoute.data.subscribe(data => this.jefes = data.jefes);
  }

  ngOnInit() {
  }

  navegarAtras() {
    if (this.visualizandoHistorial) {
      this.visualizandoHistorial = false;
    } else {
      this.router.navigateByUrl(`/home/${this.usuario}`);
    }
  }

  verHistorial(solicitud: SolicitudMantenimiento) {
    this.visualizandoHistorial = true;
    this.solicitud = solicitud;
  }

}
