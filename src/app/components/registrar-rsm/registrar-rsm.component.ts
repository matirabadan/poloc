import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JefeProyecto } from 'src/app/clases/JefeProyecto';
import { GestorRegistrarResolucionDeSM } from 'src/app/services/GestorRegistrarResolucionDeSM';
import { SolicitudMantenimiento } from 'src/app/clases/SolicitudMantenimiento';

@Component({
  selector: 'app-registrar-rsm',
  templateUrl: './registrar-rsm.component.html',
  styleUrls: ['./registrar-rsm.component.css']
})
export class RegistrarRSMComponent implements OnInit {

  usuario: string;

  jefes: JefeProyecto[] = [];

  mostrandoSolicitud = false;

  jefe: JefeProyecto;

  idSolicitud: string;

  buscado = false;

  constructor(private activatedRoute: ActivatedRoute, private solicitudesService: GestorRegistrarResolucionDeSM, private router: Router) {
    this.buscado = false;
    this.mostrandoSolicitud = false;
    this.activatedRoute.params.subscribe(data => this.usuario = data['usr']);

    this.activatedRoute.data.subscribe((data: { jefes: JefeProyecto[] }) => {
      this.jefes = data.jefes;
    })
  }

  ngOnInit() {
    console.log(this.jefes);

  }


  cargarSM(id: string, usuarioLogueado: string) {
    if (id.length > 0) {
      this.idSolicitud = id;
    }
    this.buscado = true;

    this.cargarJefes();

    this.jefes = this.solicitudesService.cargarSM(id, this.usuario);
  }

  cargarJefes() {
    this.jefes = this.solicitudesService.getJefes();
  }

  navegarAtras() {
    if (this.mostrandoSolicitud) {
      this.cargarJefes();
      this.mostrandoSolicitud = false;
    } else {
      this.router.navigateByUrl(`/home/${this.usuario}`);
    }
  }

  navegarASolicitud(jefe: JefeProyecto, id: string) {
    this.mostrandoSolicitud = true;
    this.cargarJefes();
    this.jefe = this.solicitudesService.buscarSM(id);
    this.idSolicitud = id;
  }

  getEnEjecucion() {

    let solicitudesEnEjecucion: SolicitudMantenimiento[] = [];

    for (let i = 0; i < this.jefes.length; i++) {
      let proyectos = this.jefes[i].getProyectosQueDirige();
      if (this.jefes[i].getUsuario().getNombreUsuario() === this.usuario) {
        for (let j = 0; j < proyectos.length; j++) {
          let solicitudes = proyectos[j].getSolicitudes();
          for (let k = 0; k < solicitudes.length; k++) {
            if (solicitudes[k].buscarHistorialActual().getEstado().getNombre() === 'EnEjecucion') {
              solicitudesEnEjecucion.push(solicitudes[k]);
            }
          }
        }
      }
    }

    return solicitudesEnEjecucion;
  }



}
