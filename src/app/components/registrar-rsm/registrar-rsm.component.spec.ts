import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarRSMComponent } from './registrar-rsm.component';

describe('RegistrarRSMComponent', () => {
  let component: RegistrarRSMComponent;
  let fixture: ComponentFixture<RegistrarRSMComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrarRSMComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarRSMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
