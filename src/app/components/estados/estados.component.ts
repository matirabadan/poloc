import { Component, OnInit, Input } from '@angular/core';
import { SolicitudMantenimiento } from 'src/app/clases/SolicitudMantenimiento';

@Component({
  selector: 'app-estados',
  templateUrl: './estados.component.html',
  styleUrls: ['./estados.component.css']
})
export class EstadosComponent implements OnInit {

  @Input() solicitud:SolicitudMantenimiento;

  constructor() { }

  ngOnInit() {
  }


}
