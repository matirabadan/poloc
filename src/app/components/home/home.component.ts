import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  usuario:string = "";

  constructor(private router:Router,private activatedRoute:ActivatedRoute) { 
    this.activatedRoute.params.subscribe(data=>this.usuario = data['usr'])
  }

  ngOnInit() {
  }

  finalizarSM(){
    this.router.navigateByUrl(`/regRSM/${this.usuario}`);
  }

  listarSolicitudes(){
    this.router.navigateByUrl(`/listado/${this.usuario}`);
  }

  registrarSM(){
    this.router.navigateByUrl(`/regSM/${this.usuario}`);
  }

  generarReporteSM(){
    this.router.navigateByUrl(`/reporteSM/${this.usuario}`);
  }

}
