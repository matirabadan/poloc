import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Ticket } from 'src/app/clases/Ticket';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.css']
})
export class TicketComponent implements OnInit {

  usuario:string;
  idSolicitud:string;
  @Input() tickets:Ticket[];

  constructor(private activatedRoute:ActivatedRoute, private router:Router) {
    this.activatedRoute.params.subscribe(data=>{
      this.usuario = data['usr'];   
      this.idSolicitud = data['idSolicitud']
    })
   }

  ngOnInit() {
  }

}
