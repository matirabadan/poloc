import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarSMComponent } from './registrar-sm.component';

describe('RegistrarSMComponent', () => {
  let component: RegistrarSMComponent;
  let fixture: ComponentFixture<RegistrarSMComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrarSMComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarSMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
