import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-registrar-sm',
  templateUrl: './registrar-sm.component.html',
  styleUrls: ['./registrar-sm.component.css']
})
export class RegistrarSMComponent implements OnInit {

  usuario:string;
  confirmando=false;

  constructor(private activatedRoute:ActivatedRoute, private router:Router) {
    this.confirmando = false;
    this.activatedRoute.params.subscribe(data=>{
      this.usuario = data['usr'];
    })
   }

  ngOnInit() {
  }

  navegarAtras(){
    this.router.navigateByUrl(`/home/${this.usuario}`);
  }

  confirmar(){
    this.confirmando = true;
  }

}
