import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: string = "";
  usuarioValido = true;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  iniciarSesion() {


    if (this.usuario.length == 0) {
      this.usuario = ""
    } else {
      if (this.usuario != 'lbarale' && this.usuario != 'gvelez') {
        this.usuarioValido = false;

      } else {
        this.router.navigateByUrl(`/home/${this.usuario}`);
      }
    }
  }

  setUsuario(usuario: string) {
    this.usuario = usuario;
  }

}
