import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteSMComponent } from './reporte-sm.component';

describe('ReporteSMComponent', () => {
  let component: ReporteSMComponent;
  let fixture: ComponentFixture<ReporteSMComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteSMComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteSMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
