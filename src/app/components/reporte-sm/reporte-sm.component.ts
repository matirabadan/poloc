import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-reporte-sm',
  templateUrl: './reporte-sm.component.html',
  styleUrls: ['./reporte-sm.component.css']
})
export class ReporteSMComponent implements OnInit {

  usuario:string;
  confirmando = false;
  visualizando = false;

  constructor(private activatedRoute:ActivatedRoute,private router:Router) {
    this.confirmando = false;
    this.visualizando = false;
    this.activatedRoute.params.subscribe(data=>{
      this.usuario = data['usr'];
    })
   }

  ngOnInit() {
  }

  navegarAtras(){
    this.router.navigateByUrl(`/home/${this.usuario}`);
  }

  confirmar(){
    this.confirmando = true;
  }

  visualizar(){
    this.confirmando = false;
    this.visualizando = true;
  }

}
