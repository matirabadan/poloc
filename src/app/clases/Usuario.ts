export class Usuario {
    private nombreUsuario: string;
    private password: string;

    constructor(nombre: string) {
        this.nombreUsuario = nombre;
    }

    public getNombreUsuario(): string {
        return this.nombreUsuario;
    }
}