import { TipoSM } from './TipoSM';
import { CriticidadSM } from './CriticidadSM';
import { Solicitante } from './Solicitante';
import { Plan } from './Plan';
import { HistoriaEstadoSM } from './HistoriaEstadoSM';

export class SolicitudMantenimiento {

    private identificadorSM: string;
    private nombre: string;
    private fechaCreacion: Date;
    private tipo: TipoSM;
    private fechaNecesidad: Date;
    private descripcion: string;
    private archivosAdjuntos: any;
    private criticidad: CriticidadSM;
    private creador: Solicitante;
    private estimacion: any;
    private estados: HistoriaEstadoSM[] = [];
    private plan: Plan[];
    private diagramaDeGantt: any;
    private solucionImplementada: string;
    private reclamo: any;


    constructor(identificadorSM: string, nombre: string, fechaCreacion: Date, tipo: TipoSM, fechaNecesidad: Date, descripcion: string, archivos: any, criticidad: CriticidadSM, solicitante: Solicitante, plan: Plan[]) {
        this.identificadorSM = identificadorSM;
        this.nombre = nombre;
        this.fechaCreacion = fechaCreacion;
        this.tipo = tipo;
        this.fechaNecesidad = fechaNecesidad;
        this.descripcion = descripcion;
        this.archivosAdjuntos = archivos;
        this.criticidad = criticidad;
        this.creador = solicitante;
        this.plan = plan;
    }

    getIdentificador() {
        return this.identificadorSM;
    }

    getNombre() {
        return this.nombre;
    }

    getFechaCreacion() {
        return this.fechaCreacion;
    }

    getDescripcion() {
        return this.descripcion;
    }

    getTipoSM() {
        return this.tipo;
    }

    getCriticidadSM() {
        return this.criticidad;
    }

    getSolicitante() {
        return this.creador;
    }

    getFechaNecesidad() {
        return this.fechaNecesidad;
    }

    getUltimoPlan() {
        for (let i = 0; i < this.plan.length; i++) {
            if (this.plan[i].esUltimoPlan()) {
                return this.plan[i];
            }
        }
    }


    public resolverSM(fechaActual: Date) {

        let historialActual = this.buscarHistorialActual();

        let estadoActual = historialActual.getEstado();

        if (estadoActual.esEnEjecucion()) {
            estadoActual.resolverSM(this, fechaActual, historialActual);
        }

    }

    public buscarHistorialActual(): HistoriaEstadoSM {

        for (let i = 0; i < this.estados.length; i++) {
            if (this.estados[i].esHistorialActual()) {
                return this.estados[i];
            }
        }
    }

    public setHistorialEstado(he: HistoriaEstadoSM) {
        this.estados.push(he);
    }

    public getHistorialEstado() {
        return this.estados;
    }
}
