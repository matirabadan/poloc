import { Version } from './Version';

export class Aplicacion {
    private nombre: string;
    private versiones: Version[];

    constructor(nombre: string, versiones: Version[]) {
        this.nombre = nombre;
        this.versiones = versiones;
    }

  getNombre(): string {
    return this.nombre;
  }

  setNombre(value: string) {
    this.nombre = value;
  }

  getVersiones(): Version[] {
    return this.versiones;
  }

  setVersiones(value: Version[]) {
    this.versiones = value;
  }
}
