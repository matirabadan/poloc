import {EstadoSM} from './EstadoSM';
import {SolicitudMantenimiento} from '../SolicitudMantenimiento';
import {HistoriaEstadoSM} from '../HistoriaEstadoSM';

export class Notificada extends EstadoSM {
  esAnulada(): boolean {
    return false;
  }

  esCancelada(): boolean {
    return false;
  }

  esCerrada(): boolean {
    return false;
  }

  esConfirmada(): boolean {
    return false;
  }

  esEnEjecucion(): boolean {
    return false;
  }

  esEnPrueba(): boolean {
    return false;
  }

  esEstimada(): boolean {
    return false;
  }

  esGenerada(): boolean {
    return false;
  }

  esNotificada(): boolean {
    return true;
  }

  esPlanificada(): boolean {
    return false;
  }

  esReclamada(): boolean {
    return false;
  }

  esResuelta(): boolean {
    return false;
  }

  resolverSM(s: SolicitudMantenimiento, fechaActual: Date, hea: HistoriaEstadoSM): void {
  }



}
