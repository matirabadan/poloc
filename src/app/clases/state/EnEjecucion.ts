import { EstadoSM } from './EstadoSM';
import { SolicitudMantenimiento } from '../SolicitudMantenimiento';
import { Resuelta } from './Resuelta';
import { HistoriaEstadoSM } from '../HistoriaEstadoSM';

export class EnEjecucion extends EstadoSM {
  esAnulada(): boolean {
    return false;
  }

  esCancelada(): boolean {
    return false;
  }

  esCerrada(): boolean {
    return false;
  }

  esConfirmada(): boolean {
    return false;
  }

  esEnEjecucion(): boolean {
    return true;
  }

  esEnPrueba(): boolean {
    return false;
  }

  esEstimada(): boolean {
    return false;
  }

  esGenerada(): boolean {
    return false;
  }

  esNotificada(): boolean {
    return false;
  }

  esPlanificada(): boolean {
    return false;
  }

  esReclamada(): boolean {
    return false;
  }

  esResuelta(): boolean {
    return false;
  }

  resolverSM(s: SolicitudMantenimiento, fechaActual: Date, hea: HistoriaEstadoSM): void {

    // creo el nuevo estado

    let ne: EstadoSM = new Resuelta();

    // creo el nuevo historial de estado

    let nhe: HistoriaEstadoSM = new HistoriaEstadoSM(fechaActual, ne);

    // seteo el nuevo historial a la solicitud

    s.setHistorialEstado(nhe);

    // cierro el historial anterior

    hea.setFechaHoraHasta(fechaActual);

  }

}
