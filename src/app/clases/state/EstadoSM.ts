import { SolicitudMantenimiento } from '../SolicitudMantenimiento';
import { HistoriaEstadoSM } from '../HistoriaEstadoSM';

export abstract class EstadoSM {

    private nombre: string;

    public abstract esPlanificada(): boolean;

    public abstract esEstimada(): boolean;

    public abstract esCancelada(): boolean;

    public abstract esCerrada(): boolean;

    public abstract esGenerada(): boolean;

    public abstract esEnEjecucion(): boolean;

    public abstract esResuelta(): boolean;

    public abstract esConfirmada(): boolean;

    public abstract esAnulada(): boolean;

    public abstract esReclamada(): boolean;

    public abstract esEnPrueba(): boolean;

    public abstract esNotificada(): boolean;

    public getNombre(): string {
        return this.constructor.name;
    }

    public abstract resolverSM(s: SolicitudMantenimiento, fechaActual: Date, hea: HistoriaEstadoSM): void;


}
