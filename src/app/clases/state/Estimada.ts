import {EstadoSM} from './EstadoSM';
import {SolicitudMantenimiento} from '../SolicitudMantenimiento';
import {HistoriaEstadoSM} from '../HistoriaEstadoSM';

export class Estimada extends EstadoSM {

  esAnulada() {
    return false;
  }

  esCancelada() {
    return false;
  }

  esCerrada() {
    return false;
  }

  esConfirmada() {
    return false;
  }

  esEnEjecucion() {
    return false;
  }

  esEnPrueba() {
    return false;
  }

  esEstimada() {
    return true;
  }

  esGenerada() {
    return false;
  }

  esNotificada() {
    return false;
  }

  esPlanificada() {
    return false;
  }

  esReclamada() {
    return false;
  }

  esResuelta() {
    return false;
  }

  resolverSM(s: SolicitudMantenimiento, fechaActual: Date, hea: HistoriaEstadoSM): void {
  }


}
