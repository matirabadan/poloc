export class CriticidadSM {
    private nombre: string;
    private descripcion: string;

    constructor(nombre: string, descripcion: string) {
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    getNombre() {
        return this.nombre;
    }
}