import { Usuario } from './Usuario';

export class MiembroEquipo {

    private nombre: string
    private apellido: string
    private correoElectronico: string;
    private rol: any;
    private usuario: Usuario;

    constructor(nombre: string, apellido: string) {
        this.nombre = nombre;
        this.apellido = apellido;
    }

    getNombre() {
        return this.nombre;
    }

    getApellido() {
        return this.apellido;
    }
}