import { SolicitudMantenimiento } from './SolicitudMantenimiento';
import { Cliente } from './Cliente';
import { Version } from './Version';

export class Proyecto {
    private nombre: string;
    private nombreClave: string;
    private cliente: Cliente;
    private aplicacion: Version;
    private fechaAlta: Date;
    private solicitudes: SolicitudMantenimiento[];

    constructor(nombre: string, nombreClave: string, cliente: Cliente, aplicacion: Version, fechaAlta: Date, solicitudes: SolicitudMantenimiento[]) {
        this.nombre = nombre;
        this.nombreClave = nombreClave;
        this.cliente = cliente;
        this.aplicacion = aplicacion;
        this.fechaAlta = fechaAlta;
        this.solicitudes = solicitudes;
    }

    public getNombre() {
        return this.nombre;
    }

    public getNombreClave() {
        return this.nombreClave;
    }

    public getCliente() {
        return this.cliente;
    }

    public getSolicitudes() {
        return this.solicitudes;
    }

    public setSolicitudes(solicitudes: SolicitudMantenimiento[]) {
        this.solicitudes = solicitudes;
    }

    public getAplicacion() {
        return this.aplicacion;
    }

    public getFechaAlta() {
        return this.fechaAlta;
    }
}