import { Usuario } from './Usuario';
import { Proyecto } from './Proyecto';

export class Solicitante {

    private nombre: string;
    private apellido: string;
    private tipoDocumento: any;
    private numeroDocumento: number;
    private areaOGerencia: string;
    private direccion: string;
    private rolEnArea: string;
    private correoElectronicoInstitucional: string;
    private correoElectronicoPersonal: string;
    private telefonoContacto: number;
    private proyectos: Proyecto[];
    private usuario: Usuario;

    constructor(nombre: string, apellido: string, correoElectronico: string) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.correoElectronicoPersonal = correoElectronico;
    }

    getNombre() {
        return this.nombre;
    }

    getApellido() {
        return this.apellido;
    }

    getCorreoElectronico() {
        return this.correoElectronicoPersonal;
    }
}