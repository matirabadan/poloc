import { Tarea } from './Tarea';
import { MiembroEquipo } from './MiembroEquipo';
import { Comentario } from './Comentario';

export class Ticket {

    private idetificadorTicket: string;
    private fechaInicioPrevista: Date;
    private fechaFinPrevista: Date;
    private horasEstimadas: number;
    private detalleTicket: any;
    private tarea: Tarea;
    private asignadoA: MiembroEquipo;
    private fechaInicioReal: Date;
    private fechaFinReal: Date;
    private estados: any;
    private comentarios: Comentario[] = [];
    private archivos: any;
    private informacionAnalisis: any;
    private valorHoraReal: number;

    constructor(id: string, tarea: Tarea, miembro: MiembroEquipo, fechaInicioPrevista: Date, fechaFinPrevista: Date, horasEstimadas: number) {
        this.idetificadorTicket = id;
        this.tarea = tarea;
        this.asignadoA = miembro;
        this.fechaInicioPrevista = fechaInicioPrevista;
        this.fechaFinPrevista = fechaFinPrevista;
        this.horasEstimadas = horasEstimadas;
    }

    setFechaFin(fechaFin: Date) {
        this.fechaFinReal = fechaFin;
    }

    calcularHorasConsumidas() {
        return Math.abs(this.fechaFinReal.getTime() - this.fechaInicioReal.getTime()) / 36e5;
    }

    calcularCostoAsociado() {
        return this.calcularHorasConsumidas() * this.tarea.getValorPorHora();
    }

    getIdentificadorTicket() {
        return this.idetificadorTicket;
    }

    getTarea() {
        return this.tarea;
    }

    getMiembroAsignado() {
        return this.asignadoA;
    }

    getFechaInicioPrevista() {
        return this.fechaInicioPrevista;
    }

    getFechaFinPrevista() {
        return this.fechaFinPrevista;
    }

    getHorasEstimadas() {
        return this.horasEstimadas;
    }

    getComentarios() {
        return this.comentarios;
    }

    setComentario(comentario: Comentario) {
        this.comentarios.push(comentario);
    }

    setFechaInicioReal(fecha: Date) {
        this.fechaInicioReal = fecha;
    }

    setFechaFinReal(fecha: Date) {
        this.fechaFinReal = fecha;
    }

    getFechaFinReal() {
        return this.fechaFinReal;
    }
}