export class Tarea {

    private nombre: string;
    private descripcion: string;
    private valorPorHora: number;
    private rolEjecutor: any;

    constructor(nombre: string, valorPorHora: number) {
        this.nombre = nombre;
        this.valorPorHora = valorPorHora;
    }

    getNombre() {
        return this.nombre;
    }

    getValorPorHora() {
        return this.valorPorHora;
    }
}