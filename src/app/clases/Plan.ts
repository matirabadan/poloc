import { Ticket } from './Ticket';

export class Plan {

    private fechaCreacion: Date;
    private fechaInicioResolucion: Date;
    private fechaFinResolucion: Date;
    private trabajoARealizar: Ticket[];

    constructor(fechaCreacion: Date, fechaInicioResolucion: Date, trabajo: Ticket[]) {
        this.fechaCreacion = fechaCreacion;
        this.fechaInicioResolucion = fechaInicioResolucion;
        this.fechaFinResolucion = null;
        this.trabajoARealizar = trabajo;
    }

    getFechaCreacion() {
        return this.fechaCreacion;
    }

    getFechaInicioResolucion() {
        return this.fechaInicioResolucion;
    }

    getFechaFinResolucion() {
        return this.fechaFinResolucion;
    }

    //calcula la fecha fin de acuerdo a la mayor fecha de los tickets
    setFechaFinResolucion() {

        let tickets = this.getTickets();

        let mayor = tickets[0];

        for (let i = 0; i < tickets.length; i++) {
            if (tickets[i] > mayor) {
                mayor = tickets[i];
            }
        }

        this.fechaFinResolucion = mayor.getFechaFinReal();

    }

    esUltimoPlan() {
        return true;
    }

    getTickets() {
        return this.trabajoARealizar;
    }


}