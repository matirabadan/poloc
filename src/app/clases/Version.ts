export class Version {
    private numeroVersion: number;
    private fechaLiberacion: Date;
    private nombre: string;

    constructor(numeroVersion: number, fechaLiberacion: Date, nombre: string) {
        this.numeroVersion = numeroVersion;
        this.fechaLiberacion = fechaLiberacion;
        this.nombre = nombre;
    }

    public getNumeroVersion() {
        return this.numeroVersion;
    }

    public getNombre() {
        return this.nombre;
    }
}