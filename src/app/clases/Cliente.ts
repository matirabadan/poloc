export class Cliente {
    private nombre: string;
    private fechaAlta: Date;
    private domicilio: string;

    constructor(nombre: string, fechaAlta: Date, domicilio: string) {
        this.nombre = nombre;
        this.fechaAlta = fechaAlta;
        this.domicilio = domicilio;
    }

    public getNombre(): string {
        return this.nombre;
    }

    public getFechaAlta(): Date {
        return this.fechaAlta;
    }

    public getDomicilio(): string {
        return this.domicilio;
    }
}
