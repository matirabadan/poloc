import { Usuario } from './Usuario';
import { Proyecto } from './Proyecto';

export class JefeProyecto {
    private nombre: string;
    private apellido: string;
    private correoElectronico: string;
    private usuario: Usuario;
    private proyectosQueDirige: Proyecto[];

    constructor(nombre: string, apellido: string, correoElectronico: string, usuario: Usuario, proyectos: Proyecto[]) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.correoElectronico = correoElectronico;
        this.usuario = usuario;
        this.proyectosQueDirige = proyectos;
    }

    public getNombre() {
        return this.nombre;
    }

    public getApellido() {
        return this.apellido;
    }

    public getCorreoElectronico() {
        return this.correoElectronico;
    }

    public getProyectosQueDirige() {
        return this.proyectosQueDirige;
    }

    public getUsuario() {
        return this.usuario;
    }

    public setProyectos(p: Proyecto[]) {
        this.proyectosQueDirige = p;
    }
}