import { isNull } from 'util';
import { EstadoSM } from './state/EstadoSM';

export class HistoriaEstadoSM {

    private fechaHoraDesde: Date;
    private estado: EstadoSM;
    private fechaHoraHasta: Date;

    constructor(fechaHoraActual: Date, estado: EstadoSM) {
        this.setEstado(estado);
        this.setFechaDesde(fechaHoraActual);
        this.fechaHoraHasta = null;
    }

    public setEstado(e: EstadoSM) {
        this.estado = e;
    }

    public setFechaDesde(fechaActual: Date) {
        this.fechaHoraDesde = fechaActual;
    }

    public setFechaHoraHasta(fechaHoraActual: Date) {
        this.fechaHoraHasta = fechaHoraActual;
    }


    public esHistorialActual(): Boolean {
        return isNull(this.fechaHoraHasta);
    }

    public getEstado() {
        return this.estado;
    }

    public getFechaDesde() {
        return this.fechaHoraDesde;
    }

    public getFechaHasta() {
        return this.fechaHoraHasta;
    }

}
