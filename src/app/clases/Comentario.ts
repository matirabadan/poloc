export class Comentario {

    private fechaHora: Date;
    private comentario: string;

    constructor(fechaHora: Date, comentario: string) {
        this.fechaHora = fechaHora;
        this.comentario = comentario;
    }

    getComentario() {
        return this.comentario;
    }
}