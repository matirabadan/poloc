import { Injectable } from '@angular/core';
import { GestorRegistrarResolucionDeSM } from './GestorRegistrarResolucionDeSM';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { JefeProyecto } from '../clases/JefeProyecto';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ResolverSolicitudesService implements Resolve<JefeProyecto[]> {

  generado = false;

  constructor(private solicitudesService: GestorRegistrarResolucionDeSM) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<JefeProyecto[]> | Promise<JefeProyecto[]> | JefeProyecto[] {
    if (!this.generado) {
      this.generado = true;
      return this.solicitudesService.generarCasosDePrueba();
    }
    return this.solicitudesService.getJefes();

  }
}
