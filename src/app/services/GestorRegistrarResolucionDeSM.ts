import { Injectable } from '@angular/core';
import { Usuario } from '../clases/Usuario';
import { Cliente } from '../clases/Cliente';
import { Version } from '../clases/Version'
import { Aplicacion } from '../clases/Aplicacion';
import { TipoSM } from '../clases/TipoSM';
import { CriticidadSM } from '../clases/CriticidadSM';
import { SolicitudMantenimiento } from '../clases/SolicitudMantenimiento';
import { Proyecto } from '../clases/Proyecto';
import { JefeProyecto } from '../clases/JefeProyecto';
import { Solicitante } from '../clases/Solicitante';
import { Plan } from '../clases/Plan';
import { Ticket } from '../clases/Ticket';
import { Tarea } from '../clases/Tarea';
import { MiembroEquipo } from '../clases/MiembroEquipo';
import { Comentario } from '../clases/Comentario';
import { HistoriaEstadoSM } from '../clases/HistoriaEstadoSM';
import { Estimada } from '../clases/state/Estimada';
import { Confirmada } from '../clases/state/Confirmada';
import { Planificada } from '../clases/state/Planificada';
import { EnEjecucion } from '../clases/state/EnEjecucion';
import { Generada } from '../clases/state/Generada';

@Injectable({
  providedIn: 'root'
})
export class GestorRegistrarResolucionDeSM {

  jefes: JefeProyecto[] = [];

  fechaActual: Date;

  solicitudSeleccionada: SolicitudMantenimiento;

  constructor() {

    this.jefes = this.generarCasosDePrueba();
  }

  /**
   * Genera tres SM con sus datos completos
   * 
   * El metodo retorna un arreglo de jefe de proyecto con todos los datos completos
   * 
   */
  generarCasosDePrueba(): JefeProyecto[] {

    //creo a dos usuarios

    let usuario1 = new Usuario("gvelez");
    let usuario2 = new Usuario("lbarale");

    //creo dos clientes
    let cliente1 = new Cliente("Microsoft", new Date(), "Calle Falsa 123");
    let cliente2 = new Cliente("IBM", new Date(), "Armonk, New York");

    //creo una aplicacion y una version

    let v1 = new Version(1.0, new Date(), "Version Inicial");

    let versiones: Version[] = [];

    versiones.push(v1);

    let a1 = new Aplicacion("MS Mobile", versiones);

    //creo tipo de solicitudes

    let t1 = new TipoSM("Nuevo Requerimiento", "descripcion");
    let t2 = new TipoSM("Correctivo", "descripcion");
    let t3 = new TipoSM("Cambio", "descripcion");

    //creo criticidades

    let c1 = new CriticidadSM("Critico", "descripcion");
    let c2 = new CriticidadSM("Bloqueante", "descripcion");
    let c3 = new CriticidadSM("Medio", "descripcion");
    let c4 = new CriticidadSM("Deseado", "descripcion");

    //creo solicitante

    let solicitante1 = new Solicitante("Bill", "Gates", "billgates@gmail.com");
    let solicitante2 = new Solicitante("Berkshire", "Hathaway", "bh@gmail.com");

    //creo tareas

    let tarea1 = new Tarea("Testing", 200);
    let tarea2 = new Tarea("Correción de Código", 250);
    let tarea3 = new Tarea("Análisis", 300);

    //creo dos miembros de equipo

    let miembroEquipo1 = new MiembroEquipo("Juan", "Perez");
    let miembroEquipo2 = new MiembroEquipo("Miguel", "Fernandez");

    //creo tickets

    let ticket1 = new Ticket("t1", tarea1, miembroEquipo1, new Date("June 11, 2019"), new Date("July 11, 2019"), 730);
    ticket1.setFechaInicioReal(new Date("June 13, 2019"));
    ticket1.setFechaFinReal(new Date("July 22, 2019"));

    let ticket2 = new Ticket("t2", tarea2, miembroEquipo2, new Date("January 20, 2019"), new Date("February 20, 2019"), 730);
    ticket2.setFechaInicioReal(new Date("February 1, 2019"));
    ticket2.setFechaFinReal(new Date("March 3, 2019"))

    let ticket3 = new Ticket("t3", tarea3, miembroEquipo2, new Date("September 1, 2019"), new Date("November 1, 2019"), 1060);
    ticket3.setFechaInicioReal(new Date("September 1, 2019"));
    ticket3.setFechaFinReal(new Date("November 1, 2019"));

    let ticket4 = new Ticket("t1", tarea1, miembroEquipo1, new Date("June 11, 2019"), new Date("July 11, 2019"), 730);
    ticket4.setFechaInicioReal(new Date("June 13, 2019"));
    ticket4.setFechaFinReal(new Date("July 22, 2019"));

    let ticket5 = new Ticket("t2", tarea2, miembroEquipo2, new Date("January 20, 2019"), new Date("February 20, 2019"), 730);
    ticket5.setFechaInicioReal(new Date("February 1, 2019"));
    ticket5.setFechaFinReal(new Date("March 3, 2019"))

    let ticket6 = new Ticket("t3", tarea3, miembroEquipo2, new Date("September 1, 2019"), new Date("November 1, 2019"), 1060);
    ticket6.setFechaInicioReal(new Date("September 1, 2019"));
    ticket6.setFechaFinReal(new Date("November 1, 2019"));

    let ticketsPlan1 = [ticket1, ticket2, ticket3];

    let ticketsPlan2 = [ticket4, ticket5, ticket6];

    //creo dos comentarios

    let comentario1 = new Comentario(new Date(), "Este es el primer comentario");
    let comentario2 = new Comentario(new Date(), "Este es el segundo comentario");

    ticket1.setComentario(comentario1);

    ticket2.setComentario(comentario1);
    ticket2.setComentario(comentario2);

    ticket3.setComentario(comentario2);

    ticket6.setComentario(comentario1);

    ticket4.setComentario(comentario1);
    ticket4.setComentario(comentario2);

    ticket5.setComentario(comentario2);

    //creo planes

    let plan1 = new Plan(new Date("November 1, 2018"), new Date("February 12, 2019"), ticketsPlan1);

    plan1.setFechaFinResolucion();

    let plan2 = new Plan(new Date("December 1, 2018"), new Date("January 1, 2019"), ticketsPlan2);

    plan2.setFechaFinResolucion();

    let plan3 = new Plan(new Date("December 1, 2018"), new Date("January 1, 2019"), ticketsPlan2);

    plan3.setFechaFinResolucion();

    let plan4 = new Plan(new Date("December 1, 2018"), new Date("January 1, 2019"), ticketsPlan1);

    plan4.setFechaFinResolucion();

    let planes1: Plan[] = [plan1, plan2];

    let planes2: Plan[] = [plan3, plan4]

    //creo tres solicitudes para cada usuario

    let s1 = new SolicitudMantenimiento("ProyectoY1265", "Solicitud 1", new Date("January 1, 2017"), t1, new Date("November 7, 2019"), "descripcion", null, c1, solicitante1, planes1);
    let s2 = new SolicitudMantenimiento("ProyectoY7623", "Solicitud 2", new Date("February 17, 2019"), t2, new Date("January 1, 2020"), "descripcion", null, c2, solicitante1, planes2);
    let s3 = new SolicitudMantenimiento("ProyectoY4321", "Solicitud 3", new Date("March 12, 2019"), t2, new Date("September 1, 2020"), "descripcion", null, c3, solicitante1, planes1);

    let s4 = new SolicitudMantenimiento("ProyectoX1234", "Solicitud 4", new Date("February 12, 2019"), t1, new Date("July 12, 2019"), "descripcion", null, c1, solicitante2, planes2);
    let s5 = new SolicitudMantenimiento("ProyectoX1456", "Solicitud 5", new Date("January 21, 2019"), t1, new Date("October 21, 2019"), "descripcion", null, c2, solicitante2, planes2);
    let s6 = new SolicitudMantenimiento("ProyectoX9068", "Solicitud 6", new Date("March 1, 2019"), t1, new Date("January 1, 2020"), "descripcion", null, c3, solicitante2, planes1);


    //creo historiales de estado

    let historial1s1 = new HistoriaEstadoSM(new Date("September 5, 2019"), new Generada());
    historial1s1.setFechaHoraHasta(new Date("September 12, 2019"));
    let historial2s1 = new HistoriaEstadoSM(new Date("September 12, 2019"), new Estimada());
    historial2s1.setFechaHoraHasta(new Date("September 29, 2019"));
    let historial3s1 = new HistoriaEstadoSM(new Date("September 29, 2019"), new Confirmada());
    historial3s1.setFechaHoraHasta(new Date("November 2, 2019"));
    let historial4s1 = new HistoriaEstadoSM(new Date("November 2, 2019"), new Planificada());
    historial4s1.setFechaHoraHasta(new Date("November 6, 2019"));
    let historial5s1 = new HistoriaEstadoSM(new Date("November 6, 2019"), new EnEjecucion());

    let historial1s2 = new HistoriaEstadoSM(new Date("September 5, 2019"), new Generada());
    historial1s2.setFechaHoraHasta(new Date("September 12, 2019"));
    let historial2s2 = new HistoriaEstadoSM(new Date("September 12, 2019"), new Estimada());
    historial2s2.setFechaHoraHasta(new Date("September 29, 2019"));
    let historial3s2 = new HistoriaEstadoSM(new Date("September 29, 2019"), new Confirmada());
    historial3s2.setFechaHoraHasta(new Date("November 2, 2019"));
    let historial4s2 = new HistoriaEstadoSM(new Date("November 2, 2019"), new Planificada());
    historial4s2.setFechaHoraHasta(new Date("November 6, 2019"));
    let historial5s2 = new HistoriaEstadoSM(new Date("November 6, 2019"), new EnEjecucion());

    let historial1s3 = new HistoriaEstadoSM(new Date("September 5, 2019"), new Generada());
    historial1s3.setFechaHoraHasta(new Date("September 12, 2019"));
    let historial2s3 = new HistoriaEstadoSM(new Date("September 12, 2019"), new Estimada());
    historial2s3.setFechaHoraHasta(new Date("September 29, 2019"));
    let historial3s3 = new HistoriaEstadoSM(new Date("September 29, 2019"), new Confirmada());
    historial3s3.setFechaHoraHasta(new Date("November 2, 2019"));
    let historial4s3 = new HistoriaEstadoSM(new Date("November 2, 2019"), new Planificada());
    historial4s3.setFechaHoraHasta(new Date("November 6, 2019"));
    let historial5s3 = new HistoriaEstadoSM(new Date("November 6, 2019"), new EnEjecucion());

    let historial1s4 = new HistoriaEstadoSM(new Date("September 5, 2019"), new Generada());
    historial1s4.setFechaHoraHasta(new Date("September 12, 2019"));
    let historial2s4 = new HistoriaEstadoSM(new Date("September 12, 2019"), new Estimada());
    historial2s4.setFechaHoraHasta(new Date("September 29, 2019"));
    let historial3s4 = new HistoriaEstadoSM(new Date("September 29, 2019"), new Confirmada());
    historial3s4.setFechaHoraHasta(new Date("November 2, 2019"));
    let historial4s4 = new HistoriaEstadoSM(new Date("November 2, 2019"), new Planificada());
    historial4s4.setFechaHoraHasta(new Date("November 6, 2019"));
    let historial5s4 = new HistoriaEstadoSM(new Date("November 6, 2019"), new EnEjecucion());

    let historial1s5 = new HistoriaEstadoSM(new Date("September 5, 2019"), new Generada());
    historial1s5.setFechaHoraHasta(new Date("September 12, 2019"));
    let historial2s5 = new HistoriaEstadoSM(new Date("September 12, 2019"), new Estimada());
    historial2s5.setFechaHoraHasta(new Date("September 29, 2019"));
    let historial3s5 = new HistoriaEstadoSM(new Date("September 29, 2019"), new Confirmada());
    historial3s5.setFechaHoraHasta(new Date("November 2, 2019"));
    let historial4s5 = new HistoriaEstadoSM(new Date("November 2, 2019"), new Planificada());
    historial4s5.setFechaHoraHasta(new Date("November 6, 2019"));
    let historial5s5 = new HistoriaEstadoSM(new Date("November 6, 2019"), new EnEjecucion());

    let historial1s6 = new HistoriaEstadoSM(new Date("September 5, 2019"), new Generada());
    historial1s6.setFechaHoraHasta(new Date("September 12, 2019"));
    let historial2s6 = new HistoriaEstadoSM(new Date("September 12, 2019"), new Estimada());
    historial2s6.setFechaHoraHasta(new Date("September 29, 2019"));
    let historial3s6 = new HistoriaEstadoSM(new Date("September 29, 2019"), new Confirmada());
    historial3s6.setFechaHoraHasta(new Date("November 2, 2019"));
    let historial4s6 = new HistoriaEstadoSM(new Date("November 2, 2019"), new Planificada());
    historial4s6.setFechaHoraHasta(new Date("November 6, 2019"));
    let historial5s6 = new HistoriaEstadoSM(new Date("November 6, 2019"), new EnEjecucion());


    //cargo los historiales

    s1.setHistorialEstado(historial1s1);
    s1.setHistorialEstado(historial2s1);
    s1.setHistorialEstado(historial3s1);
    s1.setHistorialEstado(historial4s1);
    s1.setHistorialEstado(historial5s1);

    s2.setHistorialEstado(historial1s2);
    s2.setHistorialEstado(historial2s2);
    s2.setHistorialEstado(historial3s2);
    s2.setHistorialEstado(historial4s2);
    s2.setHistorialEstado(historial5s2);

    s3.setHistorialEstado(historial1s3);
    s3.setHistorialEstado(historial2s3);
    s3.setHistorialEstado(historial3s3);
    s3.setHistorialEstado(historial4s3);
    s3.setHistorialEstado(historial5s3);

    s4.setHistorialEstado(historial1s4);
    s4.setHistorialEstado(historial2s4);
    s4.setHistorialEstado(historial3s4);
    s4.setHistorialEstado(historial4s4);
    s4.setHistorialEstado(historial5s4);

    s5.setHistorialEstado(historial1s5);
    s5.setHistorialEstado(historial2s5);
    s5.setHistorialEstado(historial3s5);
    s5.setHistorialEstado(historial4s5);
    s5.setHistorialEstado(historial5s5);

    s6.setHistorialEstado(historial1s6);
    s6.setHistorialEstado(historial2s6);
    s6.setHistorialEstado(historial3s6);
    s6.setHistorialEstado(historial4s6);
    s6.setHistorialEstado(historial5s6);


    //creo dos arreglos para solicitudes

    let solicitudesProyecto1: SolicitudMantenimiento[] = [];

    let solicitudesProyecto2: SolicitudMantenimiento[] = [];

    solicitudesProyecto1.push(s1, s2, s3);

    solicitudesProyecto2.push(s4, s5, s6);


    //creo dos proyectos

    let proyecto1 = new Proyecto("Promocion Alumnos", "Proyecto Y", cliente1, v1, new Date("October 31, 2019"), solicitudesProyecto1);

    let proyecto2 = new Proyecto("Examen Facil", "Proyecto X", cliente2, v1, new Date("September 7, 2019"), solicitudesProyecto2);

    let proyectos1: Proyecto[] = [];
    let proyectos2: Proyecto[] = [];

    proyectos1.push(proyecto1);

    proyectos2.push(proyecto2);



    //lleno los datos de dos jefes

    let jefe1 = new JefeProyecto("German", "Velez", "germanVelez@digitalADM.com.ar", usuario1, proyectos1);

    let jefe2 = new JefeProyecto("Lorena", "Barale", "lorenaBarale@digitalADM.com.ar", usuario2, proyectos2);

    const jefes: JefeProyecto[] = [];

    jefes.push(jefe1, jefe2);

    this.jefes = jefes;

    return jefes;

  }

  cargarSM(id: string, usuarioLogueado: string) {

    let j: JefeProyecto[] = this.clonarJefes();

    for (let i = 0; i < this.jefes.length; i++) {
      if (this.jefes[i].getUsuario().getNombreUsuario() === usuarioLogueado) {
        let proyectosGlobal: Proyecto[] = [];
        let proyectos = j[i].getProyectosQueDirige();
        for (let k = 0; k < proyectos.length; k++) {
          let solicitudesGlobal: SolicitudMantenimiento[] = [];
          let solicitudes = proyectos[k].getSolicitudes();
          for (let l = 0; l < solicitudes.length; l++) {
            if (solicitudes[l].getIdentificador().toLowerCase().includes(id.toLowerCase())) {
              solicitudesGlobal.push(solicitudes[l]);
            }
          }

          proyectos[k].setSolicitudes(solicitudesGlobal);

          proyectosGlobal.push(proyectos[k]);
        }
        j[i].setProyectos(proyectosGlobal);
      }
    }
    return j;
  }

  /**
   * Hace un deep clone del arreglo de jefes para que funcione correctamente la busqueda
   */
  clonarJefes() {

    let jefesActual = this.jefes;

    let n = jefesActual.length;

    let jefesClon: JefeProyecto[] = [];

    for (let i = 0; i < n; i++) {
      let jefeClon = new JefeProyecto(jefesActual[i].getNombre(), jefesActual[i].getApellido(), jefesActual[i].getCorreoElectronico(),
        new Usuario(jefesActual[i].getUsuario().getNombreUsuario()), null);
      let proyectos = jefesActual[i].getProyectosQueDirige();
      let proyectosClon: Proyecto[] = [];
      let solicitudesClon: SolicitudMantenimiento[] = [];
      for (let j = 0; j < proyectos.length; j++) {
        let proyectoClon = new Proyecto(proyectos[j].getNombre(), proyectos[j].getNombreClave(),
          new Cliente(proyectos[j].getCliente().getNombre(), proyectos[j].getCliente().getFechaAlta(), proyectos[j].getCliente().getDomicilio()),
          proyectos[j].getAplicacion(), proyectos[j].getFechaAlta(), null);
        let solicitudes = proyectos[j].getSolicitudes();

        for (let k = 0; k < solicitudes.length; k++) {
          let solicitudClon = new SolicitudMantenimiento(solicitudes[k].getIdentificador(), null, solicitudes[k].getFechaCreacion(), new TipoSM(solicitudes[k].getTipoSM().getNombre(), ""), solicitudes[k].getFechaNecesidad(),
            solicitudes[k].getDescripcion(), null, new CriticidadSM(solicitudes[k].getCriticidadSM().getNombre(), ""),
            new Solicitante(solicitudes[k].getSolicitante().getNombre(), solicitudes[k].getSolicitante().getApellido(), solicitudes[k].getSolicitante().getCorreoElectronico()), null);
            solicitudClon.setHistorialEstado(solicitudes[k].buscarHistorialActual());
          solicitudesClon.push(solicitudClon);
        }
        proyectoClon.setSolicitudes(solicitudesClon);
        proyectosClon.push(proyectoClon);
      }
      jefeClon.setProyectos(proyectosClon);
      jefesClon.push(jefeClon);

    }
    return jefesClon;
  }

  /**
   * 
   * Devuelve una solicitud cuyo identificador == id
   * 
   * @param id el identificador de la solicitud
   */

  buscarSM(id: string): JefeProyecto {
    let j = this.jefes;
    for (let i = 0; i < j.length; i++) {
      let proyectos = this.jefes[i].getProyectosQueDirige();
      for (let k = 0; k < proyectos.length; k++) {
        let solicitudes = proyectos[k].getSolicitudes();
        for (let l = 0; l < solicitudes.length; l++) {
          if (solicitudes[l].getIdentificador() === id) {
            this.solicitudSeleccionada = solicitudes[l];
            return j[i];
          }
        }
      }
    }
  }

  getJefes() {
    return this.jefes;
  }

  /**
   * APLICACIÓN DEL PATRON STATE
   *
   */

  finalizarSM() {

    this.fechaActual = this.obtenerFechaActual();

    this.solicitudSeleccionada.resolverSM(this.fechaActual);
  }


  obtenerFechaActual(): Date {
    return new Date();
  }

}
